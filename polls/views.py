from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
# from django.template import loader
# from django.http import Http404
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Question, Choice

# def index(request):
#     # return HttpResponse("Hello, world. You're at the polls index.")

# 	# latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     # output = ', '.join([q.question_text for q in latest_question_list])

# 	latest_question_list = Question.objects.order_by('-pub_date')[:5]
# 	# template = loader.get_template('polls/index.html')
# 	context = {
# 		'latest_question_list' : latest_question_list,
# 	}
# 	# return HttpResponse(template.render(context,request))

# 	return render(request, 'polls/index.html', context)

# def detail(request, question_id):
# 	# return HttpResponse("Question No. %s" % question_id)

# 	# try:
# 	# 	question = Question.objects.get(pk=question_id)
# 	# except Question.DoesNotExist:
# 	# 	raise Http404("Question does not exist")
# 	# return render(request, 'polls/detail.html',
# 	# 	{'question' : question})
# 	print(request.method)
# 	if request.method == "GET":
# 		question = get_object_or_404(Question, pk=question_id)
# 		return render(request, 'polls/detail.html',
# 			{'question' : question})

# 	else:
# 		pass

# def results(request, question_id):
# 	# response = "Results of question %s."
# 	# return HttpResponse(response % question_id)
# 	question = get_object_or_404(Question, pk=question_id)
# 	return render(request, 'polls/results.html', {'question' : question})

# <----------------GENERIC VIEW------------------------->
class IndexView(LoginRequiredMixin, generic.ListView):
	template_name = 'polls/index.html'
	context_object_name = 'latest_question_list'

	def get_queryset(self):
		# return Question.objects.order_by('-pub_date')[:5]
		return Question.objects.filter(
			pub_date__lte=timezone.now()
			).order_by('-pub_date')[:5]

class DetailView(LoginRequiredMixin, generic.DetailView):
	model = Question
	template_name = 'polls/detail.html'

	def get_queryset(self):
		return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(LoginRequiredMixin, generic.DetailView):
	model = Question
	template_name = 'polls/results.html'	

@login_required(login_url='/accounts/login/')
def vote(request, question_id):
	# return HttpResponse("You voted question %s." % question_id)
	question = get_object_or_404(Question, pk=question_id)
	try:
		selected_choice = question.choice_set.get(pk=request.POST['choice'])
	except (KeyError, Choice.DoesNotExist):
		return render(request, 'polls/detail.html', {
			'question' : question, 
			'error_message' : "You did not select a choice",
			})
	else:
		selected_choice.votes += 1
		selected_choice.save()

		return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
