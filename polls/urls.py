# from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    # url(r'^$', views.IndexView.as_view(), name='index'),
    # url(r'^(?P<int:pk>\d+)/$', views.DetailView.as_view(), name='detail'),
    # url(r'^(?P<int:pk>\d+)/results$', views.ResultsView.as_view(), name='results'),
    # url(r'^(?P<int:question_id>\d+)/vote$', views.vote, name='vote'),
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]