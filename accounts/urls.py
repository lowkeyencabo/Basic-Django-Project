from django.conf.urls import url

from . import views

app_name = 'accounts'

urlpatterns = [
	url(r'^home/$', views.home_view, name='home'),
	url(r'^signup/$', views.signup_view, name='signup'),
	url(r'^login/$', views.login_view, name='login'),
	url(r'^logout/$', views.logout_view, name='logout'),
]